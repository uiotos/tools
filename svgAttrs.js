const fs = require('fs');
const readline = require('readline');
const path = require('path');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function traverseDir(dirPath, forcedColor = null) {
    fs.readdir(dirPath, (err, files) => {
        if (err) {
            console.error(`读取目录${dirPath}出错，${err}`);
            process.exit(1);
        }

        files.forEach((fileName) => {
            const filePath = path.join(dirPath, fileName);
            if (fs.statSync(filePath).isFile() && fileName.endsWith('.json')) {
                console.log(`正在处理：${filePath}`);
                //对json图标文件做处理，固定尺寸、遍历绑定
                let data = fs.readFileSync(filePath, 'utf8');
                let obj = JSON.parse(data);
                // 修改key-value
                obj.background = undefined;
                obj.dataBindings = [
                  {
                    "attr": "icon-background",
                    "valueType": "Color"
                  },
                  {
                    "attr": "icon-borderWidth",
                    "valueType": "PositiveNumber"
                  },
                  {
                    "attr": "icon-borderColor",
                    "valueType": "Color"
                  }
                ];
                //固定宽高
                let ratiotmp = 45 / obj.width;
                obj.width = 45;   
                obj.height = 45;
                //
                //获取属性值
                function __attrVal(attrVal,colorType = true){
                  if(attrVal){
                    if(typeof attrVal.value == 'object')
                    return __attrVal(attrVal.value);
                    else return colorType && forcedColor ? forcedColor : (attrVal.value ? attrVal.value : (typeof attrVal == 'object' ? undefined : attrVal));
                  }else return undefined;
                }
                obj.comps.forEach(item=>{
                  //调整缩放尺寸，避免图标尺寸变化后，矢量图尺寸不变导致不协调！加上if(ratiotmp != 1) 是为了允许重入执行修改传入参数，避免重入先后有影响！
                  if(ratiotmp != 1) item.scaleX = ratiotmp;
                  if(ratiotmp != 1) item.scaleY = ratiotmp;
                  item.anchorX = 0;
                  item.anchorY = 0;
                  
                  item.borderColor = {
                      "func": "attr@icon-borderColor",
                      "value": __attrVal(item.borderColor)
                  }   
                  let wtmp =  __attrVal(item.borderWidth,false);
                  item.borderWidth = {
                      "func": "attr@icon-borderWidth",
                      "value": wtmp === undefined ? 0 : wtmp
                  }
                  item.background = {
                    "func": "attr@icon-background",
                    "value": __attrVal(item.background)
                  }
                })
                // 将修改后的数据保存回原文件
                fs.writeFileSync(filePath, JSON.stringify(obj), 'utf8');
            } else if (fs.statSync(filePath).isDirectory()) {
                traverseDir(filePath,forcedColor);
            }
        });
    });
}

rl.question('请输入目录路径：', (dirPath) => {
    console.log(`正在遍历目录：${dirPath}`);
    rl.question('指定图标颜色（可选）：', (color) => {
        traverseDir(dirPath,color);
        rl.close();
    });
});


